! =============================== !
!  2D INCOMPRESSIBLE FLOW SOLVER  !
! =============================== !
! Author: Venkata Krisshna
! EMEC 592 - HPC & Multiphase Flows
! This program is a 2D Incompressible Flow Solver

program incompressiblesolver

  implicit none
  include 'mpif.h'
  include 'silo_f9x.inc'

  integer, parameter :: SP = kind(1.0)
  integer, parameter :: DP = kind(1.0d0)
  integer, parameter :: WP = DP

  integer ierr,rank,status(MPI_STATUS_SIZE),numproc,old_comm,new_comm,ndims,reorder,dim_size(2),coord(2),dims(2),nsteps
  integer nx,ny,dimeqx,dimeqy,nx_,ny_,nxy_,procx,procy,dvar,step,dbfile,gszlr,gszud
  integer proccount,iter,up,down,left,right,imin_,imax_,jmin_,jmax_,imax,imin,jmax,jmin,i,j,count,piter,icount,jcount
  integer usevof,maincount
  real(WP) lx,ly,dx,dy,dxi,dyi,rho,rhoi,t,dt,dt_cfl,dti,nu,alpha_num,alpha_den,alpha,beta,beta_num,tolsum,maxp,maxv,maxt
  real(WP) CFLreq,CFLmax,maxres,maxresnew,maxtol,tolp,term,utop,vtop,ubot,vbot,uleft,uright,vleft,vright,tolv
  real(WP) mxtl,mytl,mxtr,mytr,mxbl,mybl,mxbr,mybr,mx,my,rdrop,xdrop,ydrop,div_o,conv
  real(WP),dimension(:,:,:),allocatable :: vs_,v_,v0_,veld
  real(WP),dimension(:,:),allocatable :: p_,p0_,div_,p_buf,ilap,vof,plic_m,plic_a
  real(WP),dimension(:),allocatable :: ghostof,rhs_,ps_,res_,resnew_,pk_,lap_,xmesh_,ymesh_
  real(WP),dimension(:),allocatable :: uxmesh_,vymesh_
  logical periods(0:1)
  character(len=50) :: siloname,dirname,vofcase,pressuremethod

  ! ======================================= !
  !  DEFINING PROBLEM AND INPUT PARAMETERS  !
  ! ======================================= !
  ! Number of processors
  procx  = 2             ! Processors in x
  procy  = 2             ! Processors in y
  ! Domain geometry
  lx     = 1             ! Length of the domain in x
  ly     = 1             ! Length of the domain in y
  nx     = 100           ! Number of nodes in x direction
  ny     = 100           ! Number of nodes in y direction
  ! Fluid Properties
  rho    = 1.0_WP        ! Fluid density (kg/m^3)
  nu     = 1.0_WP        ! Kinematic viscosity (m^2/s)
  ! Velocity Boundary Conditions
  utop   = 0.01_WP       ! u at top wall
  ubot   = 0.0_WP        ! u at bottom wall
  uright = 0.0_WP        ! u at right wall
  uleft  = 0.0_WP        ! u at left wall
  vtop   = 0.0_WP        ! v at top wall
  vbot   = 0.0_WP        ! v at bottom wall
  vright = 0.0_WP        ! v at right wall
  vleft  = 0.0_WP        ! v at left wall
  ! Pressure-solver parameters
  pressuremethod = 'cg'  ! Pressure solver method
  tolp    = 1e-4_WP      ! Required tolerance for pressure field
  ! Computational parameters
  dt      = 1e-7_WP      ! Timestep
  nsteps  = 0            ! Total number of steps
  CFLreq  = 0.1          ! Max allowable CFL
  tolv    = 1e-4_WP      ! Required convergence criterion for velocity field
  term    = 10           ! End time
  ! VOF Input Conditions
  usevof  = 0
  vofcase = 'halftank'   ! VOF input case
  rdrop   = 0.05         ! Droplet radius
  xdrop   = 0.5          ! Droplet center in x
  ydrop   = 0.5          ! Droplet center in y

  ! ===================== !
  !  MAIN SOLVER ROUTINE  !
  ! ===================== !
  call friends                 ! Initiating parallel environment
  call fire_it_up              ! Initiate program output on command window
  call buy_crayons             ! Initiate SILO output routine
  call buy_canvas              ! Initiate and distribute grid
  call pick_colors             ! Allocate computational arrays
  call fill_it_up              ! Initiate fluid region in domain
  do maincount = 1,1
     call prophecy             ! Predictor step
     call sometimes_works      ! Pressure solver
     call wife                 ! Corrector step
     call please_be_tiny       ! Compute divergence
     call shake_it_up          ! Update fluid interface
     call track_progress       ! Track progress and output data
     call paint_for_me_please  ! Write data to SILO file
     call WHEN_DO_I_STAHP      ! Check for steady state
     if(conv.lt.tolv) exit
     if(t.gt.term) exit
  end do
  if(rank.eq.0) print*,'SILO file ready for viewing!'
  if(rank.eq.0) print*,'PROGRAM COMPLETE!'
  call MPI_FINALIZE(ierr)

contains

  ! =============================================== !
  !  POISSON EQUATION SOLVER - GAUSS SEIDEL METHOD  !
  ! =============================================== !
  subroutine gspressuresolver
    p_buf = p_
    piter = 0
    do 
       piter = piter + 1
       do j = jmin_,jmax_
          do i = imin_,imax_
             p_buf(i,j) = ((p_buf(i-1,j)+p_buf(i+1,j))*(dy**2) + (p_buf(i,j-1)+p_buf(i,j+1))*(dx**2)&
                  - (rho*dti*(((vs_(0,i+1,j)-vs_(0,i,j))*dxi)+((vs_(1,i,j+1)-vs_(1,i,j))*dyi)))&
                  *(dx**2)*(dy**2))/(((dx**2)+(dy**2))*2.0_WP)
          end do
       end do
       call tolerance(p_buf,p0_)
       call comm_update(p_buf)
       call neumann(p_buf)
       if(abs(maxtol).lt.tolp) exit
       p0_ = p_buf
    end do
    p_ = p_buf
  end subroutine gspressuresolver

  ! ============= !
  !  COMPUTE RHS  !
  ! ============= !
  subroutine righthandside
    dvar = 1
    do j = jmin_,jmax_
       do i = imin_,imax_
          rhs_(dvar) = rho*dti*((vs_(0,i+1,j) - vs_(0,i,j))*dxi + (vs_(1,i,j+1) - vs_(1,i,j))*dyi)
          dvar = dvar + 1
       end do
    end do
  end subroutine righthandside

  ! ===================================================== !
  !  POISSON EQUATION SOLVER - CONJUGATE GRADIENT METHOD  !
  ! ===================================================== !
  subroutine cgpressuresolver
    call righthandside
    res_ = rhs_
    call MPI_ALLREDUCE(maxval(res_), maxres, 1, mpi_double_precision, mpi_max, mpi_comm_world, ierr)
    if(maxres.lt.tolp)then
       return
    else
       pk_ = res_
       piter = 0
       do count = 1,1
          piter = piter + 1
          call stack2cube(pk_)
          call comm_update(p_buf)
          call neumann(p_buf)
          call laplacian
          call spaghettify
          call MPI_ALLREDUCE(SUM(res_*res_), alpha_num, 1, mpi_double_precision, mpi_sum, mpi_comm_world, ierr)
          call MPI_ALLREDUCE(SUM(pk_ *lap_), alpha_den, 1, mpi_double_precision, mpi_sum, mpi_comm_world, ierr)
          alpha = alpha_num/alpha_den
          ps_  = ps_ + alpha*pk_
          resnew_ = res_ - alpha*lap_
          call MPI_ALLREDUCE(maxval(resnew_), maxresnew, 1, mpi_double_precision, mpi_max, mpi_comm_world, ierr)
          if(maxresnew.lt.tolp) exit
          call MPI_ALLREDUCE(SUM(resnew_*resnew_), beta_num, 1, mpi_double_precision, mpi_sum, mpi_comm_world, ierr)
          beta = beta_num/alpha_num
          pk_  = resnew_ + beta*pk_
          res_ = resnew_
          if(rank.eq.0) print*,piter,alpha,maxresnew
       end do
    end if
    call stack2cube(ps_)
    call neumann(p_buf)
    ! p_ = p_buf
  end subroutine cgpressuresolver

  ! ========================= !
  !  2-D STACK PRESSURE DATA  !
  ! ========================= !
  subroutine stack2cube(stackof)
    real(WP), dimension(1:nxy_) :: stackof
    dvar = 1
    do j = jmin_,jmax_
       do i = imin_,imax_
          p_buf(i,j) = stackof(dvar)
          dvar = dvar + 1
       end do
    end do
  end subroutine stack2cube

  ! ================ !
  !  PREDICTOR STEP  !
  ! ================ !
  subroutine prophecy
    call velocityboundary(v_)
    call comm_update(v_(0,:,:))
    call comm_update(v_(1,:,:))
    call CFL
    do j = jmin_,jmax_
       if(left.eq.-2)then
          do i = imin_+1,imax_
             vs_(0,i,j) = v_(0,i,j) + dt*(nu*(((v_(0,i-1,j) - 2*v_(0,i,j) + v_(0,i+1,j))*(dxi**2)) &
                  + ((v_(0,i,j-1) - 2*v_(0,i,j) + v_(0,i,j+1))*(dyi**2))) &
                  - ((v_(0,i,j)*(v_(0,i+1,j) - v_(0,i-1,j))*(0.5_WP*dxi)) &
                  - (0.25_WP*(v_(1,i-1,j) + v_(1,i,j) + v_(1,i-1,j+1) + &
                  v_(1,i,j+1))*(v_(0,i,j+1) - v_(0,i,j-1))*(0.5_WP*dyi))))
          end do
       else
          do i = imin_,imax_
             vs_(0,i,j) = v_(0,i,j) + dt*(nu*(((v_(0,i-1,j) - 2*v_(0,i,j) + v_(0,i+1,j))*(dxi**2)) &
                  + ((v_(0,i,j-1) - 2*v_(0,i,j) + v_(0,i,j+1))*(dyi**2))) &
                  - ((v_(0,i,j)*(v_(0,i+1,j) - v_(0,i-1,j))*(0.5_WP*dxi)) &
                  - (0.25_WP*(v_(1,i-1,j) + v_(1,i,j) + v_(1,i-1,j+1) + &
                  v_(1,i,j+1))*(v_(0,i,j+1) - v_(0,i,j-1))*(0.5_WP*dyi))))
          end do
       end if
    end do
    do i = imin_,imax_
       if(down.eq.-2)then
          do j = jmin_+1,jmax_
             vs_(1,i,j) = v_(1,i,j) + dt*(nu*((v_(1,i-1,j) - 2*v_(1,i,j) + v_(1,i+1,j))*(dxi**2) &
                  + (v_(1,i,j-1) - 2*v_(1,i,j) + v_(1,i,j+1))*(dyi**2)) &
                  - (0.25_WP*(v_(0,i,j-1) + v_(0,i  ,j) + v_(0,i+1,j-1) &
                  + v_(0,i+1,j))*(v_(1,i+1,j) - v_(1,i-1,j))*(0.5_WP*dxi) &
                  + v_(1,i,j)*(v_(1,i,j+1) - v_(1,i,j-1))*(0.5_WP*dyi)))
          end do
       else
          do j = jmin_,jmax_
             vs_(1,i,j) = v_(1,i,j) + dt*(nu*((v_(1,i-1,j) - 2*v_(1,i,j) + v_(1,i+1,j))*(dxi**2) &
                  + (v_(1,i,j-1) - 2*v_(1,i,j) + v_(1,i,j+1))*(dyi**2)) &
                  - (0.25_WP*(v_(0,i,j-1) + v_(0,i  ,j) + v_(0,i+1,j-1) &
                  + v_(0,i+1,j))*(v_(1,i+1,j) - v_(1,i-1,j))*(0.5_WP*dxi) &
                  + v_(1,i,j)*(v_(1,i,j+1) - v_(1,i,j-1))*(0.5_WP*dyi)))
          end do
       end if
    end do
    call velocityboundary(vs_)
    call comm_update(vs_(0,:,:))
    call comm_update(vs_(1,:,:))
  end subroutine prophecy

  ! ================ !
  !  CORRECTOR STEP  !
  ! ================ !
  subroutine wife
    do j = jmin_,jmax_
       if(left.eq.-2)then
          do i = imin_+1,imax_
             v_(0,i,j) = vs_(0,i,j) - dt*rhoi*(p_(i,j)-p_(i-1,j))*dxi
          end do
       else
         do i = imin_,imax_
             v_(0,i,j) = vs_(0,i,j) - dt*rhoi*(p_(i,j)-p_(i-1,j))*dxi
          end do
       end if
    end do
    do i = imin_,imax_
       if(down.eq.-2)then
          do j = jmin_+1,jmax_
             v_(1,i,j) = vs_(1,i,j) - dt*rhoi*(p_(i,j)-p_(i,j-1))*dyi
          end do
       else
          do j = jmin_,jmax_
             v_(1,i,j) = vs_(1,i,j) - dt*rhoi*(p_(i,j)-p_(i,j-1))*dyi
          end do
       end if
    end do
    call velocityboundary(v_)
    call comm_update(v_(0,:,:))
    call comm_update(v_(1,:,:))
  end subroutine wife

  ! ================================================ !
  !  POPULATE VELOCITY GHOST CELLS WITH DECLARED BC  !
  ! ================================================ !
  subroutine velocityboundary(v_buf)
    real(WP), dimension(0:1,imin_-1:imax_+1,jmin_-1:jmax_+1) :: v_buf
    if(down.eq.-2)then ! Bottom wall
       v_buf(1,:,jmin_  ) = vbot
       v_buf(0,:,jmin_-1) = v_buf(0,:,jmin_) - 2*(v_buf(0,:,jmin_) - ubot)
    end if
    if(up.eq.-2)then ! Top wall
       v_buf(1,:,jmax_+1) = vtop
       v_buf(0,:,jmax_+1) = v_buf(0,:,jmax_) - 2*(v_buf(0,:,jmax_) - utop)
    end if
    if(left.eq.-2)then ! Left wall
       v_buf(0,imin_,:  ) = uleft
       v_buf(1,imin_-1,:) = v_buf(1,imin_,:) - 2*(v_buf(1,imin_,:) - vleft)
    end if
    if(right.eq.-2)then ! Right wall
       v_buf(0,imax_+1,:) = uright
       v_buf(1,imax_+1,:) = v_buf(1,imax_,:) - 2*(v_buf(1,imax_,:) - vright)
    end if
  end subroutine velocityboundary

  ! ================================== !
  !  COMMUNICATE PRESSURE GHOST CELLS  !
  ! ================================== !
  subroutine comm_update(comm_data)
    real(WP), dimension(imin_-1:imax_+1,jmin_-1:jmax_+1) :: comm_data
    gszlr = int(size(comm_data(imax_,:)))
    gszud = int(size(comm_data(:,jmax_)))
    do proccount = 0,numproc - 1
       if(rank.eq.proccount)then
          if(right.ne.-2)then
             call MPI_SEND(comm_data(imax_,:  ), gszlr, mpi_double_precision, right, 10, old_comm, ierr)
             call MPI_RECV(comm_data(imax_+1,:), gszlr, mpi_double_precision, right, 11, old_comm, status, ierr)
          end if
          if(left.ne.-2)then
             call MPI_SEND(comm_data(imin_,:  ), gszlr, mpi_double_precision, left, 11, old_comm, ierr)
             call MPI_RECV(comm_data(imin_-1,:), gszlr, mpi_double_precision, left, 10, old_comm, status, ierr)
          end if
          if(up.ne.-2)then
             call MPI_SEND(comm_data(:,jmax_  ), gszud, mpi_double_precision, up, 12, old_comm, ierr)
             call MPI_RECV(comm_data(:,jmax_+1), gszud, mpi_double_precision, up, 21, old_comm, status, ierr)
          end if
          if(down.ne.-2)then
             call MPI_SEND(comm_data(:,jmin_  ), gszud, mpi_double_precision, down, 21, old_comm, ierr)
             call MPI_RECV(comm_data(:,jmin_-1), gszud, mpi_double_precision, down, 12, old_comm, status, ierr)
          end if
       end if
    end do
  end subroutine comm_update

  ! ================================== !
  !  COMMUNICATE VELOCITY GHOST CELLS  !
  ! ================================== !
  subroutine WHEN_DO_I_STAHP
    call tolerance(v_(0,:,:),v0_(0,:,:))
    conv = maxtol
    call tolerance(v_(1,:,:),v0_(1,:,:))
    conv = conv + maxtol
    v0_ = v_
    if(conv.lt.tolv.and.rank.eq.0) print*,'Velocity field converged!'
  end subroutine WHEN_DO_I_STAHP

  ! =================== !
  !  COMPUTE TOLERANCE  !
  ! =================== !
  subroutine tolerance(t_buf,t0)
    real(WP), dimension(imin_-1:imax_+1,jmin_-1:jmax_+1) :: t_buf,t0
    call MPI_ALLREDUCE(maxval(t_buf), maxt, 1, mpi_double_precision, mpi_max, mpi_comm_world, ierr)
    tolsum = 0.0_WP
    maxtol = 0.0_WP
    do j = jmin_,jmax_
       do i = imin_,imax_
          tolsum = tolsum + abs(t_buf(i,j)-t0(i,j))
       end do
    end do
    call MPI_ALLREDUCE(tolsum, maxtol, 1, mpi_double_precision, mpi_sum, mpi_comm_world, ierr)
    maxtol = maxtol/maxp
  end subroutine tolerance
  
  ! ============================ !
  !  SPAGHETTIFY LAPLACIAN GRID  !
  ! ============================ !
  subroutine spaghettify
    dvar = 1
    do j = jmin_,jmax_
       do i = imin_,imax_
          lap_(dvar) = ilap(i,j)
          dvar = dvar + 1
       end do
    end do
  end subroutine spaghettify

  ! =================== !
  !  COMPUTE LAPLACIAN  !
  ! =================== !
  subroutine laplacian
    do j = jmin_,jmax_
       do i = imin_,imax_
          ilap(i,j) = -(((p_(i+1,j)-(2*p_(i,j))+p_(i-1,j))*(dxi**2)) + &
               ((p_(i,j+1)-(2*p_(i,j))+p_(i,j-1))*(dyi**2)))
       end do
    end do
  end subroutine laplacian

  ! ========================= !
  !  POISSON EQUATION SOLVER  !
  ! ========================= !
  subroutine sometimes_works
    if(pressuremethod.eq.'cg') call cgpressuresolver
    if(pressuremethod.eq.'gs') call gspressuresolver
  end subroutine sometimes_works

  ! =========================================== !
  !  POPULATE DATA GHOST CELLS WITH NEUMANN BC  !
  ! =========================================== !
  subroutine neumann(n_buf)
    real(WP), dimension(imin_-1:imax_+1,jmin_-1:jmax_+1) :: n_buf
    if (down.eq.-2) n_buf(:,jmin_-1) = n_buf(:,jmin_) ! Bottom walls
    if   (up.eq.-2) n_buf(:,jmax_+1) = n_buf(:,jmax_) ! Top wall
    if (left.eq.-2) n_buf(imin_-1,:) = n_buf(imin_,:) ! Left wall
    if(right.eq.-2) n_buf(imax_+1,:) = n_buf(imax_,:) ! Right wall
  end subroutine neumann

  ! ================================ !
  !  COMPUTE DIVERGENCE OF VELOCITY  !
  ! ================================ !
  subroutine please_be_tiny
    do j = jmin_,jmax_
       do i = imin_,imax_
          div_(i,j) = (((v_(0,i+1,j) - v_(0,i,j))*dxi) + ((v_(1,i,j+1) - v_(1,i,j))*dyi))
       end do
    end do
    call MPI_ALLREDUCE(maxval(div_), div_o, 1, mpi_double_precision, mpi_max, mpi_comm_world, ierr)
  end subroutine please_be_tiny

  ! ================================= !
  !  INITIATE FLUID REGION IN DOMAIN  !
  ! ================================= !
  subroutine fill_it_up
    if(usevof.eq.1)then
       if(vofcase.eq.'halftank')then
          if(ymesh_(jmax).le.ly/2)then
             vof(imin_:imax_,jmin_:jmax_) = 1.0_WP
          else if(ymesh_(jmin).le.ly/2)then
             count = jmin
             do j = jmin_,jmax_
                if(ymesh_(count).lt.ly/2) vof(imin_:imax_,j) = 1.0_WP
                count = count + 1
             end do
          end if
       end if
       if(vofcase.eq.'fulltank') &
            vof(imin_:imax_,jmin_:jmax_) = 1.0_WP
       if(vofcase.eq.'brokendam'.and.coord(1).eq.0.and.coord(2).eq.0)then
          if(ymesh_(jmax).le.ly/2)then
             vof(int(imax_*0.25):int(imax_*0.5),jmin_:jmax_) = 1.0_WP
          else if(ymesh_(jmin).le.ly/2)then
             count = jmin
             do j = jmin_,jmax_
                if(ymesh_(count).le.ly/2) vof(int(imax_*0.25):int(imax_*0.5),j) = 1.0_WP
                count = count + 1
             end do
          end if
       end if
       if(vofcase.eq.'droplet')then
          if(rdrop.eq.0)then
             print*,'Please specify a non-zero droplet radius.'
          else
             jcount = jmin
             do j = jmin_,jmax_
                icount = imin
                do i = imin_,imax_
                   if((xmesh_(icount)-xdrop)**2+(ymesh_(jcount)-ydrop)**2.le.rdrop**2)then
                      vof(i,j) = 1.0_WP
                   end if
                   icount = icount + 1
                end do
                jcount = jcount + 1
             end do
          end if
       end if
    end if
  end subroutine fill_it_up

  ! ============= !
  !  TIME UPDATE  !
  ! ============= !
  subroutine track_progress
    if(nsteps.eq.0) step = step + 1
    t = t + dt
    if(rank.eq.0) print*,'  ',int(step),real(t,SP),'   ',real(CFLmax,SP)&
         ,real(maxval(abs(v_(0,:,:))),SP),real(abs(maxval(v_(1,:,:))),SP),abs(div_o),piter
  end subroutine track_progress

  ! ============================================ !
  !  INITIATING PROGRAM OUTPUT ON COMMAND WINDOW !
  ! ============================================ !
  subroutine fire_it_up
    step = 0
    if(rank.eq.0)then
       print*,'INITIATING SOLVER...'
       print*,' -------------------------------------------------------------------------------------',&
            '--------------------------------------------'
       print*,'|          Step   Time Elapsed           CFLmax'&
            ,'           Umax             Vmax                Divergence             P Iters     |'
       print*,' -------------------------------------------------------------------------------------',&
            '--------------------------------------------'
    end if
  end subroutine fire_it_up

  ! =============== !
  !  CFL CONDITION  !
  ! =============== !
  subroutine CFL
    dt_cfl = 9.999999999e9_WP
    call MPI_ALLREDUCE(maxval(v_), maxv, 1, mpi_double_precision, mpi_max, mpi_comm_world, ierr)
    CFLmax = maxv*dt/dx
    if(maxv.gt.0) dt_cfl = CFLreq*dx/(maxv)
    if(dt_cfl.lt.dt) dt = dt_cfl
  end subroutine CFL

  ! ====================================== !
  !  UPDATE FLUID CONFIGURATION IN DOMAIN  !
  ! ====================================== !
  subroutine shake_it_up
    if(usevof.eq.1)then
       call neumann(vof)
       call commVOF
       call PLIC_normal
       call PLIC_alpha
    end if
  end subroutine shake_it_up

  ! =========================== !
  !  PLIC NORMAL DETERMINATION  !
  ! =========================== !
  subroutine PLIC_normal
    do j = jmin_,jmax_
       do i = imin_,imax_
          mxtr = -(vof(i+1,j+1)+vof(i+1,j)-vof(i,j+1)-vof(i,j))*0.5_WP*dxi
          mytr = -(vof(i+1,j+1)-vof(i+1,j)+vof(i,j+1)-vof(i,j))*0.5_WP*dxi
          mxtl = -(vof(i,j+1)+vof(i,j)-vof(i-1,j+1)-vof(i-1,j))*0.5_WP*dxi
          mytl = -(vof(i,j+1)-vof(i,j)+vof(i-1,j+1)-vof(i-1,j))*0.5_WP*dxi
          mxbl = -(vof(i+1,j)+vof(i+1,j-1)-vof(i,j)-vof(i,j-1))*0.5_WP*dxi
          mybl = -(vof(i+1,j)-vof(i+1,j-1)+vof(i,j)-vof(i,j-1))*0.5_WP*dxi
          mxbr = -(vof(i,j)+vof(i,j-1)-vof(i-1,j)-vof(i-1,j-1))*0.5_WP*dxi
          mybr = -(vof(i,j)-vof(i,j-1)+vof(i-1,j)-vof(i-1,j-1))*0.5_WP*dxi
          mx = 0.25_WP*(mxtr + mxtl + mxbr + mxbl)
          my = 0.25_WP*(mytr + mytl + mybr + mybl)
          plic_m(i,j) = sqrt(mx**2 + my**2)
       end do
    end do
  end subroutine PLIC_normal

  ! ========================== !
  !  PLIC ALPHA DETERMINATION  !
  ! ========================== !
  subroutine PLIC_alpha
    do j = jmin_,jmax_
       do i = imin_,imax_
          if((vof(i,j)*dx*dy).ge.0.and.vof(i,j)*dx*dy.lt.(mx/(2.0_WP*my))) &
               plic_a(i,j) = sqrt(2.0_WP*mx*my*vof(i,j)*dx*dy)
          if((vof(i,j)*dx*dy).ge.(mx/(2.0_WP*my)).and.vof(i,j)*dx*dy.lt.0.5_WP) &
               plic_a(i,j) = my*vof(i,j)*dx*dy + mx*0.5_WP
       end do
    end do
  end subroutine PLIC_alpha

  ! ============================= !
  !  COMMUNICATE VOF GHOST CELLS  !
  ! ============================= !
  subroutine commVOF
    if(numproc.gt.1)then
       gszlr = int(size(vof(imin_,:)))
       gszud = int(size(vof(:,jmin_)))
       do proccount = 0,numproc - 1
          if(rank.eq.proccount)then
             if(right.ne.-2)then
                call MPI_SEND(vof(imax_,:), gszlr, mpi_double_precision, right, 10, old_comm, ierr)
                call MPI_RECV(vof(imax_+1,:), gszlr, mpi_double_precision, right, 20, old_comm, status, ierr)
             end if
             if(left.ne.-2)then
                call MPI_SEND(vof(imin_,:), gszlr, mpi_double_precision, left, 20, old_comm, ierr)
                call MPI_RECV(vof(imin_-1,:), gszlr, mpi_double_precision, left, 10, old_comm, status, ierr)
             end if
             if(up.ne.-2)then
                call MPI_SEND(vof(:,jmax_), gszud, mpi_double_precision, up, 90, old_comm, ierr)
                call MPI_RECV(vof(:,jmax_+1), gszud, mpi_double_precision, up, 80, old_comm, status, ierr)
             end if
             if(down.ne.-2)then
                call MPI_SEND(vof(:,jmin_), gszud, mpi_double_precision, down, 80, old_comm, ierr)
                call MPI_RECV(vof(:,jmin_-1), gszud, mpi_double_precision, down, 90, old_comm, status, ierr)
             end if
          end if
       end do
    end if
  end subroutine commVOF

  ! =============================== !
  !  INITIATE PARALLEL ENVIRONMENT  !
  ! =============================== !
  subroutine friends
    call MPI_INIT(ierr)
    call MPI_COMM_RANK(mpi_comm_world,rank,ierr)
    call MPI_COMM_SIZE(mpi_comm_world,numproc,ierr)
    if(numproc.ne.procx*procy.and.rank.eq.0) print*,'INCORRECT NUMBER OF PROCESSORS SPECIFIED!'
    old_comm    = MPI_COMM_WORLD
    ndims       = 2          ! 2D matrx/grid
    dim_size(1) = procy      ! rows
    dim_size(2) = procx      ! columns
    periods(0)  = .false.    ! row-periodic (each column wraps around)
    periods(1)  = .false.    ! column-nonperiodic
    reorder     = 0
    ! Creating Cartesian grid for parallel computing
    call MPI_CART_CREATE(old_comm, ndims, dim_size, periods, reorder, new_comm, ierr)
    call MPI_CART_COORDS(new_comm, rank, ndims, coord, ierr)
    call MPI_CART_SHIFT(new_comm, 0, 1, up, down, ierr)
    call MPI_CART_SHIFT(new_comm, 1, 1, left, right, ierr)
    ! print*,rank,up,down,left,right
  end subroutine friends

  ! ================= !
  !  ALLOCATE ARRAYS  !
  ! ================= !
  subroutine pick_colors
    ! 2D arrays
    allocate(v_(0:1,imin_-1:imax_+1,jmin_-1:jmax_+1),vs_(0:1,imin_-1:imax_+1,jmin_-1:jmax_+1))
    allocate(v0_(0:1,imin_-1:imax_+1,jmin_-1:jmax_+1))
    allocate(veld(0:1,imin_-1:imax_+1,jmin_-1:jmax_+1),div_(imin_:imax_,jmin_:jmax_))
    allocate(ilap(imin_:imax_,jmin_:jmax_))
    allocate(p_(imin_-1:imax_+1,jmin_-1:jmax_+1),p_buf(imin_-1:imax_+1,jmin_-1:jmax_+1))
    allocate(p0_(imin_-1:imax_+1,jmin_-1:jmax_+1),vof(imin_-1:imax_+1,jmin_-1:jmax_+1))
    allocate(plic_m(imin_:imax_,jmin_:jmax_),plic_a(imin_:imax_,jmin_:jmax_))
    ! Column arrays
    allocate(lap_(1:nxy_),ps_(1:nxy_),res_(1:nxy_),rhs_(1:nxy_),resnew_(1:nxy_),pk_(1:nxy_),ghostof(1:nxy_))
    ! Initializing to zero
    v_      = 0
    vs_     = 0
    v0_     = 0
    veld    = 0
    div_    = 0
    p_      = 0
    p_buf   = 0
    p0_     = 0
    vof     = 0
    ilap    = 0
    plic_m  = 0
    plic_a  = 0
    ghostof = 0
    rhs_    = 0
    lap_    = 0
    ps_     = 0
    pk_     = 0
    res_    = 0
    lap_    = 0
    resnew_ = 0
  end subroutine pick_colors

  ! ================= !
  !  DISTRIBUTE GRID  !
  ! ================= !
  subroutine buy_canvas
    dx  = lx/nx    ! Distance between two adjacent nodes in x
    dy  = ly/ny    ! Distance between two adjacent nodes in y
    dxi = 1/dx
    dyi = 1/dy
    dti = 1/dt
    rhoi = 1/rho
    ! Splitting the job
    dimeqx = nx - mod(nx,procx)
    dimeqy = ny - mod(ny,procy)
    nx_  = dimeqx/procx
    ny_  = dimeqy/procy
    if(right.eq.-2)then
       nx_ = nx_ + mod(nx,procx)
    end if
    if(down.eq.-2)then
       ny_ = ny_ + mod(ny,procy)
    end if
    nxy_  = (nx_+1)*(ny_+1)
    imin_ = 0               ! Lower bound in x for each processor
    imax_ = nx_             ! Upper bound in x for boundary processors
    jmin_ = 0               ! Lower bound in y for each processor
    jmax_ = ny_             ! Upper bound in y for boundary processors
    imin = coord(2)*dimeqx/procx
    imax = imin + nx_
    jmin = (procy-coord(1))*dimeqy/procy
    jmax = jmin + ny_
    ! print*,'rank -',rank,'imin -',imin_,'imax -',imax_,'jmin -',jmin_,'jmax -',jmax_,'nx -',nx_,'ny -',ny_
    ! print*,'rank -',rank,'imin -',imin,'imax -',imax,'jmin -',jmin,'jmax -',jmax!,'nx -',nx,'ny -',ny
    allocate(xmesh_(imin:imax+1), ymesh_(jmin:jmax+1), uxmesh_(imin:imax+2), vymesh_(jmin:jmax+2))
    do i = imin,imax+1
       xmesh_(i) = i*dx
    end do
    do j = jmin,jmax+1
       ymesh_(j) = j*dy
    end do
    do i = imin,imax+2
       uxmesh_(i) = i*dx - 0.5*dx
    end do
    do j = jmin,jmax+2
       vymesh_(j) = j*dy - 0.5*dy
    end do
    t = 0                   ! Initializing time
  end subroutine buy_canvas

  ! =============== !
  !  INITIATE SILO  !
  ! =============== !
  subroutine buy_crayons
    if(rank.eq.0) call execute_command_line('rm -fr Visit/*')
    if(rank.eq.0) call execute_command_line('mkdir -p Visit')
  end subroutine buy_crayons

  ! ==================== !
  !  WRITE TO SILO FILE  !
  ! ==================== !
  subroutine paint_for_me_please
    ! Create the silo database
    write(siloname,'(A,I0.5,A)') 'Visit/step',step,'.silo'
    if(rank.eq.0)then
       ierr = dbcreate(siloname,len_trim(siloname),DB_CLOBBER,DB_LOCAL,"Silo database",13,DB_HDF5,dbfile)
       if(dbfile.eq.-1) print *,'Could not create Silo file!'
       ierr = dbclose(dbfile)
    end if
    call MPI_BARRIER(MPI_COMM_WORLD,ierr)
    ! Each processor writes its data
    do proccount=0,numproc-1
       if(rank.eq.proccount)then
          ierr = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)      ! Open silo file
          write(dirname,'(I5.5)')rank             ! Create directory inside silo file for this proc
          ierr = dbmkdir(dbfile,dirname,5,ierr)
          ierr = dbsetdir(dbfile,dirname,5)
          dims(1) = size(xmesh_)
          dims(2) = size(ymesh_)
          ! Write quad-mesh for this processor
          ierr = dbputqm(dbfile,'Mesh',4,'xc',2,'yc',2,'zc',2,xmesh_,ymesh_,DB_F77NULL,&
               dims,2,DB_DOUBLE,DB_COLLINEAR,DB_F77NULL,ierr)
          ierr = dbputqm(dbfile,'uMesh',5,'xc',2,'yc',2,'zc',2,uxmesh_,ymesh_,DB_F77NULL,&
               (/size(uxmesh_),size(ymesh_)/),2,DB_DOUBLE,DB_COLLINEAR,DB_F77NULL,ierr)
          ierr = dbputqm(dbfile,'vMesh',5,'xc',2,'yc',2,'zc',2,xmesh_,vymesh_,DB_F77NULL,&
               (/size(xmesh_),size(vymesh_)/),2,DB_DOUBLE,DB_COLLINEAR,DB_F77NULL,ierr)
          ! Write data for this processor
          ierr = dbputqv1(dbfile,'U',1,'uMesh',5,v_(0,imin_:imax_+1,jmin_:jmax_),&
               (/size(uxmesh_)-1,size(ymesh_)-1/),2,DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
          ierr = dbputqv1(dbfile,'Us',2,'uMesh',5,vs_(0,imin_:imax_+1,jmin_:jmax_),&
               (/size(uxmesh_)-1,size(ymesh_)-1/),2,DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
          ierr = dbputqv1(dbfile,'V',1,'vMesh',5,v_(1,imin_:imax_,jmin_:jmax_+1),&
               (/size(xmesh_)-1,size(vymesh_)-1/),2,DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
          ierr = dbputqv1(dbfile,'Vs',2,'vMesh',5,vs_(1,imin_:imax_,jmin_:jmax_+1),&
               (/size(xmesh_)-1,size(vymesh_)-1/),2,DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
          ierr = dbputqv1(dbfile,'P',1,'Mesh',4,p_(imin_:imax_,jmin_:jmax_),[dims(1)-1,dims(2)-1],&
               2,DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
          ierr = dbputqv1(dbfile,'Divg',4,'Mesh',4,div_(imin_:imax_,jmin_:jmax_),[dims(1)-1,dims(2)-1],&
               2,DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
          ierr = dbputqv1(dbfile,'VOF',3,'Mesh',4,vof(imin_:imax_,jmin_:jmax_),[dims(1)-1,dims(2)-1],&
               2,DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
          ierr = dbputqv1(dbfile,'Norm',4,'Mesh',4,plic_m(imin_:imax_,jmin_:jmax_),[dims(1)-1,dims(2)-1],&
               2,DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
          ierr = dbputqv1(dbfile,'Alpha',5,'Mesh',4,plic_a(imin_:imax_,jmin_:jmax_),[dims(1)-1,dims(2)-1],&
               2,DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
          ! Close silo file
          ierr = dbclose(dbfile)
       end if
       call MPI_BARRIER(MPI_COMM_WORLD,ierr)
    end do
    call multimeshvar
  end subroutine paint_for_me_please

  ! ======================================= !
  !  WRITE MULTIMESH/MULTIVAR TO SILO FILE  !
  ! ======================================= !
  subroutine multimeshvar
    character(len=20),dimension(numproc):: names
    integer,dimension(numproc) :: lnames,types
    if(rank.eq.0)then
       ierr = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)
       ierr = dbset2dstrlen(20)
       ! uMesh
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount-1,'/uMesh'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUAD_RECT
       end do
       ierr = dbputmmesh(dbfile,"uMesh",5,numproc,names,lnames,types,DB_F77NULL,ierr)
       ! U
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount-1,'/U'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUADVAR
       end do
       ierr = dbputmvar(dbfile,"U",1,numproc,names,lnames,types,DB_F77NULL,ierr)
       ! Us
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount-1,'/Us'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUADVAR
       end do
       ierr = dbputmvar(dbfile,"Us",2,numproc,names,lnames,types,DB_F77NULL,ierr)
       ! vMesh
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount-1,'/vMesh'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUAD_RECT
       end do
       ierr = dbputmmesh(dbfile,"vMesh",5,numproc,names,lnames,types,DB_F77NULL,ierr)
       ! V
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount-1,'/V'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUADVAR
       end do
       ierr = dbputmvar(dbfile,"V",1,numproc,names,lnames,types,DB_F77NULL,ierr)
       ! Vs
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount-1,'/Vs'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUADVAR
       end do
       ierr = dbputmvar(dbfile,"Vs",2,numproc,names,lnames,types,DB_F77NULL,ierr)
       ! Mesh
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount-1,'/Mesh'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUAD_RECT
       end do
       ierr = dbputmmesh(dbfile,"Mesh",4,numproc,names,lnames,types,DB_F77NULL,ierr)
       ! P
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount-1,'/P'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUADVAR
       end do
       ierr = dbputmvar(dbfile,"P",1,numproc,names,lnames,types,DB_F77NULL,ierr)
       ! Divergence
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount-1,'/Divg'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUADVAR
       end do
       ierr = dbputmvar(dbfile,"Divg",4,numproc,names,lnames,types,DB_F77NULL,ierr)
       ! VOF
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount-1,'/VOF'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUADVAR
       end do
       ierr = dbputmvar(dbfile,"VOF",3,numproc,names,lnames,types,DB_F77NULL,ierr)
       ! Normal vector m
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount-1,'/Norm'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUADVAR
       end do
       ierr = dbputmvar(dbfile,"Norm",4,numproc,names,lnames,types,DB_F77NULL,ierr)
       ! PLIC alpha
       do proccount=1,numproc
          write(names(proccount),'(I5.5,A)') proccount-1,'/Alpha'
          lnames(proccount)=len_trim(names(proccount))
          types(proccount)=DB_QUADVAR
       end do
       ierr = dbputmvar(dbfile,"Alpha",5,numproc,names,lnames,types,DB_F77NULL,ierr)
       ! Close the silo file
       ierr = dbclose(dbfile)
    end if
  end subroutine multimeshvar

end program incompressiblesolver
